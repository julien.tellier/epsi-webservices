package org.tpepsi.data.entity;

/**
 * Classe représentant un objet générique Item
 *
 * @author James Chollet
 * @version 1.0
 */

public abstract class Item {

    /**
     * L'identifiant de l'Item
     * <br>
     * Sera auto-incrémenté
     */
    private long id;

    /**
     * Titre de l'Item
     */
    private String title;

    /**
     * Nom de l'emprunteur (vide à linstanciation de l'Item)
     */
    private String borrowedBy;

    public Item(){
        this.id= -1;
        this.title = "";
        this.borrowedBy = "";
    }

    /**
     * Constructeur de l'Item
     * @param id
     * @param title
     */
    public Item(long id,String title){
        this.id = id;
        this.title = title;
        this. borrowedBy = "";
    }

    /**
     * Getter de l'id d'un Item
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Getter du titre d'un Item
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter de l'id d'un Item
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Setter du titre d'un Item
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter du nom de l'emprunteur
     *
     */
    public String getBorrowedBy() {
        return borrowedBy;
    }

    /**
     * Setter de l'emprunteur
     *
     * @param borrowedBy
     */
    public void setBorrowedBy(String borrowedBy) {
        this.borrowedBy = borrowedBy;
    }

    /**
     * Méthode vérifiant si l'Item est déjà emprunté
     *
     * @return boolean
     */
    public boolean isOut(){

        if (this.borrowedBy == "") {
            return false;
        }
        else {
            return true;
        }

    }

    /**
     * Méthode abstraite de recherche (doit être implémentée par les sous-classes d'Item)
     *
     * @param term
     * @return
     */
    abstract public boolean search(String term);
}