package org.tpepsi.data.entity;

/**
 * Classe représentant un objet TvShow
 *
 * @author James Chollet
 * @version 1.0
 */

public class TvShow extends Item {

    /**
     * Créateur de la Série TV
     */
    private String showrunner;

    /**
     * Chaîne sur laquelle est diffusée la Série TV
     */
    private String network;

    /**
     * Constructeur par défaut
     */
    public TvShow(){
        super();
    }

    /**
     * Constructeur de la Série TV
     *
     * @param id
     * @param title
     * @param showrunner
     * @param network
     */
    public TvShow(long id, String title, String showrunner, String network){
        super(id, title);
        this.showrunner = showrunner;
        this.network = network;
    }

    /**
     * Getter du créateur de la série
     *
     * @return String
     */
    public String getShowrunner() {
        return showrunner;
    }

    /**
     * Setter du créateur de la série
     *
     * @param showrunner
     */
    public void setShowrunner(String showrunner) {
        this.showrunner = showrunner;
    }

    /**
     * Getter de la chaîne de la série
     *
     * @return String
     */
    public String getNetwork() {
        return network;
    }

    /**
     * Setter de la chaîne de la série
     *
     * @param network
     */
    public void setNetwork(String network) {
        this.network = network;
    }

    /**
     * Implémentation de la méthode de recherche propre aux séries
     *
     * @param term
     * @return boolean
     */
    public boolean search(String term){
        if (super.getTitle().toUpperCase().contains(term.toUpperCase())){
            return true;
        }
        else if (this.showrunner.toUpperCase().contains(term.toUpperCase())){
            return true;
        }
        else if (this.network.toUpperCase().contains(term.toUpperCase())){
            return true;
        }
        else {
            return false;
        }
    }
}