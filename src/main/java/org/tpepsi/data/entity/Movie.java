package org.tpepsi.data.entity;

/**
 * Classe représentant un objet Movie
 *
 * @author James Chollet
 * @version 1.0
 */

public class Movie extends Item {

    /**
     * Réalisateur du film
     */
    private String director;

    /**
     * Année de sortie du film
     */
    private int releaseYear;

    /**
     * Constructeur par défaut
     */
    public Movie(){
        super();
    }

    /**
     * Constructeur du film
     *
     * @see Item
     * @param id
     * @param title
     * @param director
     * @param releaseYear
     */
    public Movie(long id,String title, String director, int releaseYear){
        super(id, title);
        this.director = director;
        this.releaseYear = releaseYear;
    }

    /**
     * Getter du réalisateur
     *
     * @return String
     */
    public String getDirector(){
        return director;
    }


    /**
     * Setter du réalisateur
     *
     * @param director
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * Getter de l'année de sortie
     *
     * @return int
     */
    public int getReleaseYear() {
        return releaseYear;
    }

    /**
     * Setter de l'année de sortie
     *
     * @param releaseYear
     */
    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    /**
     * Implémentation de la méthode de recherche propre aux films
     *
     * @param term
     * @return boolean
     */
    public boolean search(String term){
        if (super.getTitle().toUpperCase().contains(term.toUpperCase())){
            return true;
        }
        else if (this.director.toUpperCase().contains(term.toUpperCase())){
            return true;
        }
        else {
            return false;
        }
    }
}