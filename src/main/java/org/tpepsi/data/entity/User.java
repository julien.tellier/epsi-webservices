package org.tpepsi.data.entity;

/**
 * Classe représentant un objet User
 * <br>
 * Uniquement utilisée pour l'emprunt et le rendu d'articles
 *
 * @author Julien Tellier
 * @version 1.0
 */
public class User {

    private String username;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
