package org.tpepsi.service;
import org.tpepsi.data.entity.Movie;
import org.springframework.stereotype.Service;

/**
 * Service de CRUD des instances de Movie
 * <br>
 * Nécessaire à la généricité et à la différenciation des différentes listes d'Items
 */
@Service
public class MovieService extends ItemService<Movie>{}
