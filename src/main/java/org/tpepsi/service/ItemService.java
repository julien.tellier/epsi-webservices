package org.tpepsi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.tpepsi.data.entity.Item;
import org.tpepsi.data.entity.User;
import org.tpepsi.exceptions.AllItemsAlreadyReturnedException;
import org.tpepsi.exceptions.ItemNotFoundException;
import org.tpepsi.exceptions.UnavailableItemException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Service de CRUD des instances de Movie
 *
 * @Author Julien Tellier
 * @version 1.0
 */
@Service
public class ItemService<T extends Item>{
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final AtomicLong counter = new AtomicLong();
    private final List<T> itemList = new ArrayList<>();

    /**
     * Ajout d'un Item
     *
     * @param item
     * @return Item
     */
    public T addItem(T item){

        item.setId(counter.incrementAndGet());

        itemList.add(item);
        return item;
    }

    /**
     * Liste des Items
     *
     * @return List<Item>
     */
    public List<T> getItems(){

        return itemList;
    }

    /**
     * Récupération d'un Item par son id
     *
     * @param id
     * @return Optional<Item>
     */
    public Optional<T> getItem(long id){
        for (Item i: itemList) {
            if(i.getId() == id){
                // return Optional.of(i);
                return Optional.of((T) i);
            }
        }
        return Optional.empty();
    }

    /**
     * Emprunt d'un Item
     *
     * @param id
     * @param user
     * @param type
     * @throws UnavailableItemException
     * @throws ItemNotFoundException
     */
    public void borrowItem(long id, User user, String type) throws UnavailableItemException, ItemNotFoundException {
        boolean found = false;
        try {
            for(Item i : itemList) {
                if (i.getId() == id) {
                    if (i.isOut()) {
                        throw new UnavailableItemException("This " + type + " is unavailable");
                    } else {
                        found = true;
                        i.setBorrowedBy(user.getUsername());
                        break;
                    }
                }
            }
            if (!found) {
                throw new ItemNotFoundException("This " + type + " doesn't exist");
            }
        }
        catch (UnavailableItemException e){
            log.error(e.getMessage());
        }
        catch (ItemNotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Retour d'un Item
     *
     * @param id
     * @param user
     * @param type
     * @throws ItemNotFoundException
     * @throws AllItemsAlreadyReturnedException
     */
    public void returnItem(long id, User user, String type) throws ItemNotFoundException, AllItemsAlreadyReturnedException {
        boolean found = false;
        String message;
        try {
            for (Item i : itemList) {
                if (i.getId() == id) {
                    if (!i.isOut()) {
                        message = i.getTitle() + " was already returned";
                        throw new AllItemsAlreadyReturnedException(message);

                    } else {
                        found=true;
                        i.setBorrowedBy("");
                        message = user.getUsername() + " returned " + i.getTitle();
                        log.info(message);
                    }
                }
            }
            if (!found) {
                throw new ItemNotFoundException("This " + type + " doesn't exist");
            }
        }
        catch(AllItemsAlreadyReturnedException e)
        {
            log.error(e.getMessage());
        }
        catch(ItemNotFoundException e)
        {
            log.error(e.getMessage());
        }
    }

    /**
     * Liste d'Items répondant à un critère de recherche
     *
     * @param searchTerm
     * @return List<Item>
     */
    public List<Item> search(String searchTerm){
        List<Item> resultList = new ArrayList();

        for(Item i: itemList){
            if (i.search(searchTerm)){
                resultList.add(i);
            }
        }

        return resultList;
    }
}
