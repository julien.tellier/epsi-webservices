package org.tpepsi.service;
import org.tpepsi.data.entity.TvShow;
import org.springframework.stereotype.Service;

/**
 * Service de CRUD des instances de TvShow
 *  <br>
 * Nécessaire à la généricité et à la différenciation des différentes listes d'Items
 */
@Service
public class TvShowService extends ItemService<TvShow>{}
