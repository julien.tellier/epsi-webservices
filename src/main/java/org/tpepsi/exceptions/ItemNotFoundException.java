package org.tpepsi.exceptions;

/**
 * Déclaration de l'exception ItemNotFoundException
 * <br>
 * Lancée quand l'on essaie de rendre/emprunter un Item inexistant
 */
public class ItemNotFoundException extends Exception {

    public ItemNotFoundException(String message){
        super(message);
    }



}
