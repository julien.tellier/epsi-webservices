package org.tpepsi.exceptions;

/**
 * Déclaration de l'exception AllItemsAlreadyReturnedException
 * <br>
 * Lancée quand l'on essaie de rendre un Item qui a déjà été rendu
 */
public class AllItemsAlreadyReturnedException extends Exception {
    public AllItemsAlreadyReturnedException() {
    }

    public AllItemsAlreadyReturnedException(String message) {
        super(message);
    }

    public AllItemsAlreadyReturnedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AllItemsAlreadyReturnedException(Throwable cause) {
        super(cause);
    }

    public AllItemsAlreadyReturnedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
