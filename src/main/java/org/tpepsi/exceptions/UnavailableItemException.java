package org.tpepsi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Déclaration de l'exception UnavailableItemException
 * <br>
 * Lancée quand l'on essaie d'emprunter un Item qui est déjà emprunté
 */
public class UnavailableItemException extends Exception{

    public UnavailableItemException() {
    }

    public UnavailableItemException(String message) {
        super(message);
    }

    public UnavailableItemException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailableItemException(Throwable cause) {
        super(cause);
    }

    public UnavailableItemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
