package org.tpepsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application principale
 */
@SpringBootApplication(scanBasePackages = {"org.tpepsi.data","org.tpepsi.controllers","org.tpepsi.service","org.tpepsi.config"})
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}