package org.tpepsi.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tpepsi.data.entity.Movie;
import org.tpepsi.data.entity.TvShow;
import org.tpepsi.data.entity.User;
import org.tpepsi.exceptions.AllItemsAlreadyReturnedException;
import org.tpepsi.exceptions.ItemNotFoundException;
import org.tpepsi.exceptions.UnavailableItemException;
import org.tpepsi.service.ItemService;

import java.util.List;
import java.util.Optional;

/**
 * Controller REST de l'objet TvShow
 *<br>
 * <a>http://localhost:8080/swagger-ui.html</a>
 *
 * @author Julien Tellier
 * @verion 1.0
 */
@RestController
@RequestMapping("/tvshow")
@Api(value="movies", description="Operations pertaining to TV Shows")
public class TvShowController {

    private ItemService<TvShow> tvShowService;
    private ItemService<Movie> movieService;
    private final String ITEM_TYPE = "TV show";

    /**
     * Constructeur du controller pour les séries
     * <br>
     * Pour pouvoir gérer à la fois une liste de Movies et de TVShows, on instancie les deux types de service
     *
     * @param movieService
     * @param tvShowService
     */
    @Autowired
    public TvShowController(ItemService<TvShow> tvShowService, ItemService<Movie> movieService){
        this.tvShowService = tvShowService;
        this.movieService = movieService;
    }

    /**
     * Mapping pour l'ajout d'une série
     * <br>
     * Envoi d'un objet TvShow (JSON) sur /movie
     *
     * @param show
     * @return TvShow
     */
    @ApiOperation(value = "Add a TV Show",response = TvShow.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added TV Show"),
            @ApiResponse(code = 401, message = "You are not authorized to add the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody TvShow addTvShow(@RequestBody TvShow show){
        return this.tvShowService.addItem(show);
    }

    /**
     * Listing des séries
     *
     * @return List<TvShow>
     */
    @ApiOperation(value = "View a list of available TV Shows",response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved TV Show list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<TvShow> getTvShows(){
        return this.tvShowService.getItems();
    }

    /**
     * Trouver une série grâce à son id
     *
     * @param tvShowId
     * @return ResponseEntity<TvShow>
     */
    @ApiOperation(value = "Search a TV Show by ID",response = TvShow.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved TV Show"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/{tvShowId}", method = RequestMethod.GET)
    public ResponseEntity<TvShow> getTvShow(@PathVariable("tvShowId") long tvShowId){

        Optional<TvShow> show = this.tvShowService.getItem(tvShowId);
        if(show.isPresent()){
            return new ResponseEntity(show.get(),HttpStatus.OK);
        }
        else{
            return new ResponseEntity(null,HttpStatus.NO_CONTENT);
        }

    }

    /**
     * Recherche avec critère d'une série
     *
     * @param searchTerm
     * @return ResponseEntity<List<TvShow>>
     */
    @ApiOperation(value = "Search a TV Show with a search term", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a movie"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/search/{searchTerm}", method = RequestMethod.GET)
    public ResponseEntity<List<TvShow>> searchTvShows(@PathVariable("searchTerm") String searchTerm) {
        return new ResponseEntity(this.tvShowService.search(searchTerm),HttpStatus.OK);

    }

    /**
     * Emprunt d'une série
     *
     * @param tvShowId
     * @param user
     * @return ResponseEntity
     * @throws ItemNotFoundException
     * @throws UnavailableItemException
     */
    @ApiOperation(value = "Borrow a TV Show")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a TV Show"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/{tvShowId}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity borrowTvShow(@PathVariable("tvShowId") long tvShowId, @RequestBody User user) throws ItemNotFoundException, UnavailableItemException {

        tvShowService.borrowItem(tvShowId,user,ITEM_TYPE);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Retour d'une série
     *
     * @param tvShowId
     * @param user
     * @return ResponseEntity
     * @throws ItemNotFoundException
     * @throws AllItemsAlreadyReturnedException
     */
    @ApiOperation(value = "Return a TV Show")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a TV Show"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/return/{tvShowId}", method = RequestMethod.POST)
    public ResponseEntity returnTvShow(@PathVariable("tvShowId") long tvShowId, @RequestBody User user) throws ItemNotFoundException, AllItemsAlreadyReturnedException {
        this.tvShowService.returnItem(tvShowId,user, ITEM_TYPE);
        return new ResponseEntity(HttpStatus.OK);
    }

}
