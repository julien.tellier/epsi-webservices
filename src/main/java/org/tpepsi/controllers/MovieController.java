package org.tpepsi.controllers;

import java.util.List;
import java.util.Optional;

import org.tpepsi.data.entity.TvShow;
import org.tpepsi.exceptions.AllItemsAlreadyReturnedException;
import org.tpepsi.exceptions.ItemNotFoundException;
import org.tpepsi.exceptions.UnavailableItemException;
import org.tpepsi.service.ItemService;
import org.tpepsi.data.entity.Movie;
import org.tpepsi.data.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller REST de l'objet Movie
 * <br>
 * <a>http://localhost:8080/swagger-ui.html</a>
 *
 * @author Julien Tellier
 * @verion 1.0
 */
@RestController
@RequestMapping("/movie")
@Api(value="movies", description="Operations pertaining to movies")
public class MovieController{

    private ItemService<Movie> movieService;
    private ItemService<TvShow> tvShowService;
    private final String ITEM_TYPE = "movie";

    /**
     * Constructeur du controller pour les films
     * <br>
     * Pour pouvoir gérer à la fois une liste de Movies et de TVShows, on instancie les deux types de service
     *
     * @param movieService
     * @param tvShowService
     */
    @Autowired
    public MovieController(ItemService<Movie> movieService,ItemService<TvShow> tvShowService){
        this.movieService = movieService;
        this.tvShowService = tvShowService;
    }

    /**
     * Mapping pour l'ajout d'un film
     * <br>
     * Envoi d'un objet Movie (JSON) sur /movie
     *
     * @param movie
     * @return Movie
     */
    @ApiOperation(value = "Add a movie",response = Movie.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added movie"),
            @ApiResponse(code = 401, message = "You are not authorized to add the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody Movie addMovie(@RequestBody Movie movie){
        return this.movieService.addItem(movie);
    }

    /**
     * Listing des films
     *
     * @return List<Movie>
     */
    @ApiOperation(value = "View a list of available movies",response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved movie list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<Movie> getMovies(){
        return this.movieService.getItems();
    }

    /**
     * Trouver un film grâce à son id
     *
     * @param movieId
     * @return ResponseEntity<Movie>
     */
    @ApiOperation(value = "Search a movie by ID",response = Movie.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved movie"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/{movieId}", method = RequestMethod.GET)
    public ResponseEntity<Movie> getMovie(@PathVariable("movieId") long movieId){

        Optional<Movie> movie = this.movieService.getItem(movieId);
        if(movie.isPresent()){
            return new ResponseEntity(movie.get(),HttpStatus.OK);
        }
        else{
            return new ResponseEntity(null,HttpStatus.NO_CONTENT);
        }

    }

    /**
     * Recherche avec critère d'un film
     *
     * @param searchTerm
     * @return ResponseEntity<List<Movie>>
     */
    @ApiOperation(value = "Search a movie with a search term", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a movie"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/search/{searchTerm}", method = RequestMethod.GET)
    public ResponseEntity<List<Movie>> searchMovies(@PathVariable("searchTerm") String searchTerm) {
        return new ResponseEntity(this.movieService.search(searchTerm),HttpStatus.OK);

    }

    /**
     * Emprunt d'un film
     *
     * @param movieId
     * @param user
     * @return ResponseEntity
     * @throws ItemNotFoundException
     * @throws UnavailableItemException
     */
    @ApiOperation(value = "Borrow a Movie")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a movie"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/{movieId}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity borrowMovie(@PathVariable("movieId") long movieId, @RequestBody User user) throws ItemNotFoundException, UnavailableItemException {

        movieService.borrowItem(movieId,user,ITEM_TYPE);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Retour d'un film
     *
     * @param movieId
     * @param user
     * @return ResponseEntity
     * @throws ItemNotFoundException
     * @throws AllItemsAlreadyReturnedException
     */
    @ApiOperation(value = "Return a Movie")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully borrowed a movie"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(value = "/return/{movieId}", method = RequestMethod.POST)
    public ResponseEntity returnMovie(@PathVariable("movieId") long movieId, @RequestBody User user) throws ItemNotFoundException, AllItemsAlreadyReturnedException {
        this.movieService.returnItem(movieId,user, ITEM_TYPE);
        return new ResponseEntity(HttpStatus.OK);
    }
}